<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use App\Entity\Article;
/**
 * Plutôt que de créer le formulaire directement dans les controleurs,
 * symfony préconise de créer des classes représentant les formulaires
 * appelés Type.
 * Ici, on fait une classe ArticleType dans laquelle on définira 
 * les différents champs du formulaire ainsi que la classe à
 * laquelle le formulaire est lié
 */
class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //On définit les champs ici
        $builder
            ->add('title', TextType::class)
            ->add('author', TextType::class)
            //->add('tags', TextType::class)
            ->add('date', DateType::class)
            //->add('viewCount', IntegerType::class)
            ->add('text', TextType::class);
            //On ne met pas les boutons submit histoire de rendre
            //le formulaire le plus réutilisable possible
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        //On indique quelle classe le formulaire permet de créer
        $resolver->setDefaults([
            "data_class" => Article::class
        ]);
    }
}
