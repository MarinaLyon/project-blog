<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Repository\ArticleRepository;


class HomeController extends AbstractController {
   
    /**
     * @Route("/home", name="home")
     */

    public function index(ArticleRepository $repo) {
        
        /* On utilise la méthode render() du controller pour dire à
        la méthode d'interpréter le template qu'on lui donne en premier argument.*/
        $result = $repo->getAll();
        return $this->render("home.html.twig", [
            "result" => $result
        ]);
    } 

}