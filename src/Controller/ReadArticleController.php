<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Admin;
use App\Entity\Article;


class ReadArticleController extends AbstractController {
   
    /**
     * @Route("/read-article", name="read-article")
     */

    public function index() {
        
        /* On utilise la méthode render() du controller pour dire à
        la méthode d'interpréter le template qu'on lui donne en premier argument.*/
        return $this->render("readArticle.html.twig", []);
    } 

}