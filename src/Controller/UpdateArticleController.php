<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;


class UpdateArticleController extends AbstractController {
   
    /**
     * @Route("/update-article/{id}", name="update-article")
     */

    public function index(Request $request, ArticleRepository $repo, int $id) {
        $article = $repo->getById($id);
        $form = $this->createForm(ArticleType::class, $article);


        //On fait la suite comme avec un formulaire normal
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //On récupère l'instance de article générée par le formulaire
            //avec getData() et on le donne à manger à la méthode
            //add du ArticleRepository qui fera persister l'article en question
            $repo->update($form->getData());
            //On fait une redirection lors d'un ajout réussi
            return $this->redirectToRoute("home");
        }


        return $this->render('articleUpdate.html.twig', [
            'form' => $form->createView()
        ]);
        
    } 

}