<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Entity\Article;


class ArticleRemoveController extends AbstractController {
   
    /**
      * @Route("/article-remove", name="articleRemove")
      */

     public function index(ArticleRepository $repo) {
        
         /* On utilise la méthode render() du controller pour dire à
         la méthode d'interpréter le template qu'on lui donne en premier argument.*/
         $result = $repo->getAll();
         return $this->render("articleRemove.html.twig", [
            "result" => $result
         ]);
     } 


    /**
     * @Route("/article-remove/{id}", name="articleRemoveId")
     */
    public function remove(int $id, ArticleRepository $repo) {
        $repo->delete($id);
        return $this->redirectToRoute("home");
    }



}