<?php

namespace App\Repository;

use App\Entity\Article;
use App\Utils\ConnectUtil;

class ArticleRepository {
  
  public function getAll() : array {

    $articles = [];
      try {
        $cnx = ConnectUtil::getConnection();
        /*
        La méthode prepare() sur une connexion PDO va mettre une requête SQL en attente d'exécution à l'intérieur d'un
        objet de type PDOStatement (qu'on stock ici dans $query)
        */
        $query = $cnx->prepare("SELECT * FROM article");
        //Pour la requête soit lancer, il faut lancer la méthode execute de l'objet query
        $query->execute();

        /*
        Pour récupérer les résultats de la requête, on peut utiliser la méthode fetchAll qui renverra un tableau
        de tableau associatif. Chaque tableau associatif représentera une ligne de résultat. On fait une boucle sur le tableau de résultat pour faire en sorte de convertir chaque ligne de résultat brut en une instance de la classe voulue (ici SmallDog)
        */
        foreach ($query->fetchAll() as $row) {
                $article = new Article();
                $article->fromSQL($row);
                $articles[] = $article;
        }

        } catch (\PDOException $e) {
            dump($e);
        }
        
        return $articles;
    }

    public function add(Article $article) {
        /** connexion à PDO à externaliser dans une classe à
        * part (ou, dans une méthode au pire)
        */
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("INSERT INTO article (title, author, date, text) VALUES (:title, :author, :date, :text)");
            
            $query->bindValue(":title", $article->title);
            $query->bindValue(":author", $article->author);
            //$query->bindValue(":tags", $article->tags);
            $query->bindValue(":date", $article->date->format("Y-m-d"));
            //$query->bindValue(":viewCount", $article->viewCount);
            $query->bindValue(":text", $article->text);

            $query->execute();

            $article->id = intval($cnx->lastInsertId());

        } catch (\PDOException $e) {
            dump($e);
        }
    }


    public function delete(int $id) {
        try {
            $cnx = ConnectUtil::getConnection();
            $query = $cnx->prepare("DELETE FROM article WHERE id=:id");
            
            $query->bindValue(":id", $id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function update(article $article) {

        try {
            $cnx = ConnectUtil::getConnection();
            $query = $cnx->prepare("UPDATE article SET title=:title, date=:date, text=:text, author=:author WHERE id=:id");
            
            $query->bindValue(":title", $article->title);
            $query->bindValue(":date", $article->date->format("Y-m-d H:i:s"));
            $query->bindValue(":text", $article->text);
            $query->bindValue(":author", $article->author);
            $query->bindValue(":id", $article->id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function getById(int $id): ?Article {
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("SELECT * FROM article WHERE id=:id");
            
            $query->bindValue(":id", $id);

            $query->execute();

            $result = $query->fetchAll();

            if(count($result) === 1) {
                $article = new Article();
                $article->fromSQL($result[0]);
                return $article;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return null;
    }

}







