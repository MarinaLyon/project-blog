<?php

namespace App\Entity;

class Article {
  public $id;
  public $title;
  public $author;
  public $date;
  public $text;

  // public function __construct(string $title = "", string $author = "", string $tags = "", \DateTime $date = null, int $viewCount = 0, string $text= "") {
  //   $this->id = $id;
  //   $this->title = $title;
  //   $this->author = $author;
  //   $this->tags = $tags;
  //   $this->date = $date;
  //   $this->viewCount = $viewCount;
  //   $this->text = $text;
  // }

/**
* Méthode qui assigne à l'instance de user les valeurs contenues
* dans un tableau associatif d'une ligne de résultat pdo.
*/
  public function fromSQL(array $sql) {
    
    $this->id = $sql["id"];
    $this->title = $sql["title"];
    $this->author = $sql["author"];
    //$this->tags = $sql["tags"];
    $this->date = \DateTime::createFromFormat('Y-m-d', $sql["date"]);
    $this->text = $sql["text"];
  }
}