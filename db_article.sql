-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `text` text,
  `author` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (6,'Monologue D\'Otis','2013-01-01','Mais, vous savez, moi je ne crois pas qu\'il y ait de bonne ou de mauvaise situation. Moi, si je devais rÃ©sumer ma vie aujourd\'hui avec vous, je dirais que c\'est d\'abord des rencontres, Des gens qui m\'ont tendu la main, peut-Ãªtre Ã  un moment oÃ¹ je ne pouvais pas, oÃ¹ j\'Ã©tais seul chez moi. Et c\'est assez curieux de se dire que les hasards, les rencontres forgent une destinÃ©e... Parce que quand on a le goÃ»t de la chose, quand on a le goÃ»t de la chose bien faite, Le beau geste, parfois on ne trouve pas l\'interlocuteur en face, je dirais, le miroir qui vous aide Ã  avancer. Alors ce n\'est pas mon cas, comme je le disais lÃ , puisque moi au contraire, j\'ai pu ; Et je dis merci Ã  la vie, je lui dis merci, je chante la vie, je danse la vie... Je ne suis qu\'amour! Et finalement, quand beaucoup de gens aujourd\'hui me disent : \"Mais comment fais-tu pour avoir cette humanitÃ© ?\", Eh bien je leur rÃ©ponds trÃ¨s simplement, je leur dis que c\'est ce goÃ»t de l\'amour, Ce goÃ»t donc qui m\'a poussÃ© aujourd\'hui Ã  entreprendre une construction mÃ©canique, Mais demain, qui sait, peut-Ãªtre simplement Ã  me mettre au service de la communautÃ©, Ã  faire le don, le don de soi...','Edouard Baer'),(8,'Citation de MaÃ®tre Yoda','2018-07-25','L\'ignorance mÃ¨ne Ã  la peur, la peur mÃ¨ne Ã  la haine, la haine mÃ¨ne Ã  la violence... VoilÃ  l\'Ã©quation.','Abu al-Walid Ibn Rushd (AverroÃ¨s)'),(9,'Petit TraitÃ© de l\'Abandon','2018-07-25','La vie n\'est jamais loupÃ©e. La vie n\'est pas Ã  rÃ©ussir. Ce n\'est pas un objectif. Vivre est Ã  soi sa propre fin.','Alexandre Jollien'),(10,'King Kong ThÃ©orie','2018-07-25','Parce que l\'idÃ©al de la femme blanche, sÃ©duisante mais pas pute, bien mariÃ©e mais pas effacÃ©e, travaillant mais sans trop rÃ©ussir, pour ne pas Ã©craser son homme, mince mais pas nÃ©vrosÃ©e par la nourriture, restant indÃ©finiment jeune sans se faire dÃ©figurer par les chirurgiens de l\'esthÃ©tique, maman Ã©panouie mais pas accaparÃ©e par les couches et les devoirs d\'Ã©cole, bonne maÃ®tresse de maison mais pas bonniche traditionnelle, cultivÃ©e mais moins qu\'un homme, cette femme blanche heureuse qu\'on nous brandit tout le temps sous le nez, celle Ã  laquelle on devrait faire l\'effort de ressembler, Ã  part qu\'elle a l\'air de beaucoup s\'emmerder pour pas grand-chose, de toutes faÃ§ons je ne l\'ai jamais croisÃ©e, nulle part. Je crois bien qu\'elle n\'existe pas.','Virginie Despentes'),(11,'La CitÃ© de la Peur','2018-07-25','On ne peut pas tromper une personne mille fois... si, si on peut tromper mille personnes une fois... euh mille fois... non, on ne peut tromper pas une fois mille personnes, Odile, mais on peut tromper une fois mille personnes, oui on ne peut pas tromper mille fois...','Emile Gravier');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-26 15:37:59
